#![cfg_attr(test, feature(test))]
use std::sync::Arc;
use std::thread;

fn main() {
    let r1 = tokio::runtime::Builder::new_multi_thread()
        .worker_threads(10)
        .thread_name("a")
        .enable_all()
        .build()
        .unwrap();
    let r2 = Arc::new(tokio::runtime::Builder::new_multi_thread()
        .worker_threads(10)
        .thread_name("b")
        .enable_all()
        .build()
        .unwrap());
    let r2c = r2.clone();
    r1.block_on(async move {
        for _ in 0..10000 {
            tokio::task::block_in_place(|| {
                // This runs on expected "b" thread
                let j = r2.spawn(async move {
                    // Breakpoint here to check which threadpool this runs on
                    println!("spawn {:?}", thread::current().name())
                });
                r2.block_on(j).unwrap();

                // This runs on the main thread?!  I guess it was recruited to run the outer
                // future.
                r2.block_on(async move {
                    // Breakpoint here to check which threadpool this runs on
                    println!("block_on {:?}", thread::current().name())
                });
            });
        }
    });
    println!("done");
}

#[cfg(test)]
mod benches {
    extern crate test;

    use std::sync::Arc;
    use test::Bencher;
    use tokio::runtime::{Builder, Handle};

    #[bench]
    fn two_runtimes_no_block_in_place(b: &mut Bencher) {
        let r1 = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .thread_name("a")
            .enable_all()
            .build()
            .unwrap();
        let r2 = Arc::new(tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .thread_name("b")
            .enable_all()
            .build()
            .unwrap());
        let r2c = r2.clone();
        r1.block_on(async move {
            tokio::task::block_in_place(|| {
                b.iter(|| {
                    let j = r2.spawn(async move {});
                    Handle::current().block_on(j).unwrap();
                });
            });
        });
    }

    #[bench]
    fn two_runtimes(b: &mut Bencher) {
        let r1 = tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .thread_name("a")
            .enable_all()
            .build()
            .unwrap();
        let r2 = Arc::new(tokio::runtime::Builder::new_multi_thread()
            .worker_threads(2)
            .thread_name("b")
            .enable_all()
            .build()
            .unwrap());
        let r2c = r2.clone();
        r1.block_on(async move {
            b.iter(|| {
                tokio::task::block_in_place(|| {
                    let j = r2.spawn(async move {});
                    Handle::current().block_on(j).unwrap();
                });
            });
        });
    }

    #[bench]
    fn current_thread_cached(b: &mut Bencher) {
        let r1 = Builder::new_multi_thread()
            .worker_threads(10)
            .thread_name("sensei")
            .enable_all()
            .build()
            .unwrap();
        let r2 = Arc::new(Builder::new_current_thread()
            .enable_io()
            .build().unwrap());
        let r2c = r2.clone();
        r1.block_on(async move {
            b.iter(|| {
                tokio::task::block_in_place(|| {
                    r2.block_on(async move {});
                });
            });
        });
    }

    #[bench]
    fn current_thread(b: &mut Bencher) {
        let r1 = Builder::new_multi_thread()
            .worker_threads(10)
            .thread_name("sensei")
            .enable_all()
            .build()
            .unwrap();
        r1.block_on(async move {
            b.iter(|| {
                tokio::task::block_in_place(|| {
                    let r2 = Builder::new_current_thread()
                        .enable_io()
                        .build().unwrap();
                    r2.block_on(async move {});
                });
            });
        });
    }
}
